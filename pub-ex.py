#!/usr/bin/python3
#
# pub-ex.py
#
# Copied from
# https://www.digikey.com/en/maker/blogs/2019/how-to-use-mqtt-with-the-raspberry-pi
# 
# This publishes data to the server to demonstrate MQTT.
#
# Seth McNeill
# 2020 Janaury 28

import paho.mqtt.client as mqtt # Import the MQTT library
import time # The time library is useful for delays

 
ourClient = mqtt.Client("CPTR487pub") # Create a MQTT client object
ourClient.connect("test.mosquitto.org", 1883) # Connect to the test MQTT broker
ourClient.loop_start() # Start the MQTT client

# Main program loop

print("starting to publish")

while(1):
#  print("Sending topic cptr487-test with message on")
  ourClient.publish("cptr487-test", "on") # Publish message to MQTT broker
  time.sleep(1) # Sleep for a second
#  print("Sending topic cptr487-test with message off")
  ourClient.publish("cptr487-test", "off") # Publish message to MQTT broker
  time.sleep(1) # Sleep for a second

