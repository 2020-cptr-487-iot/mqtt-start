#!/usr/bin/python3
#
# pahomqtt-ex.py
#
# Copied from
# https://www.digikey.com/en/maker/blogs/2019/how-to-use-mqtt-with-the-raspberry-pi
#
# This is an example subscriber for MQTT
#
# Seth McNeill
# 2020 Janaury 28

import paho.mqtt.client as mqtt # Import the MQTT library
import time # The time library is useful for delays
import pdb # python debugger
import ledshim # control the ledshim

# Our "on message" event
def messageFunction (client, userdata, message):
  topic = str(message.topic)
  message_str = str(message.payload.decode("utf-8"))
  print('Received topic: "' + topic + '" with message: ' +  message_str)
  if(message_str == 'on'):
      ledshim.set_all(255,0,255)
      ledshim.show()
  if(message_str == 'off'):
      ledshim.clear()
      ledshim.show()

 
ourClient = mqtt.Client("CPTR487sub") # Create a MQTT client object
ourClient.connect("test.mosquitto.org", 1883) # Connect to the test MQTT broker
ourClient.subscribe("cptr487-test") # Subscribe to the topic AC_unit
ourClient.on_message = messageFunction # Attach the messageFunction to subscription
ourClient.loop_start() # Start the MQTT client

# Main program loop

while(1):
  time.sleep(1) # Sleep for a second

