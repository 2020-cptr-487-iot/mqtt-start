#!/usr/bin/python3
#
# pahomqtt-ex.py
#
# Copied from
# https://www.digikey.com/en/maker/blogs/2019/how-to-use-mqtt-with-the-raspberry-pi
# Seth McNeill
# 2020 Janaury 28

import paho.mqtt.client as mqtt # Import the MQTT library
import time # The time library is useful for delays

# Our "on message" event
def messageFunction (client, userdata, message):
  topic = str(message.topic)
  message = str(message.payload.decode("utf-8"))
  print('Received topic: "' + topic + '" with message: ' +  message)

 
ourClient = mqtt.Client("CPTR487") # Create a MQTT client object
ourClient.connect("test.mosquitto.org", 1883) # Connect to the test MQTT broker
ourClient.subscribe("cptr487-test") # Subscribe to the topic AC_unit
ourClient.on_message = messageFunction # Attach the messageFunction to subscription
ourClient.loop_start() # Start the MQTT client

# Main program loop

while(1):
  print("Sending topic cptr487-test with message on")
  ourClient.publish("cptr487-test", "on") # Publish message to MQTT broker
  time.sleep(1) # Sleep for a second
  print("Sending topic cptr487-test with message off")
  ourClient.publish("cptr487-test", "off") # Publish message to MQTT broker
  time.sleep(1) # Sleep for a second

